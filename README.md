# Static website on s3 using SOPS variant

This project sample shows the usage of to be continuous templates:
* [S3](https://gitlab.com/dt-contribution-tbc/s3)
* [S3 SOPS variant](https://gitlab.com/dt-contribution-tbc/s3#sops-variant)
* [Lighthouse](https://to-be-continuous.gitlab.io/doc/ref/lighthouse/)

The application is a simple static website

## S3 template features

This project uses the following features from the GitLab CI S3 template:

* Defines mandatory `$S3_ENDPOINT_HOST`
* Set `$S3_DEPLOY_FILES` to define the output build directory as the deployable S3 resources.

The GitLab CI S3 template also implements environments integration in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related development branch is deleted).

## S3 SOPS template features

This project uses the following features from the GitLab CI S3 SOPS variant:

* Defines mandatory `$SOPS_AGE_KEY`as secret project variable

